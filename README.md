# Premise 

Ability to remotly recover a CPE modem/router, so that non-technical users need not worry about
fixing connection issues themselves. 

## Background

Relsellers of wholesale ISPs are called Communication Providers (CPs). CPs sell through a wholesaler and have 
less access to the infrastructure (little/no control over RADIUS credentials, no BRAS, DSLAM).

CPs therefore have a challenge of not being able to remotely configure their routers, which for customers is a 
pain-point and staff trying their best to support (ofen non-technical) customers.

Scenario: Customer re-sets their router, loses DSL credentials, feels sad, doesn't know how to fix it, not I.T
savy enough to fix it. 

Openxdsl is to build something that (maybe) has a Data SIM, and open router firmware which allows us
to harness that connection to configure a customers router settings remotely.

Larger providers resolve this with TR-069 which CPs can't necessarily make use of because they don't run the infrastructure,
hence our stab in the dark with a Data SIM + open router firmware (such as OpenWrt/LEDE).

Obviously there's a big question over security and privacy here, against the very real challenge of non-technical
customers not knowing how to configure their equipment and must be considered. Even the most tallented support 
staff stuggle to resolve a connection issue with a non technical user's service; it's not fun. Being able to 
resolve this remotley keeps everyones sanity, reduces friction and promotes automation.

This project is the substance behind the Best Broadband Providers Project:  this: https://blog.karmacomputing.co.uk/best-broadband-providers-project/
